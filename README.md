# prototypes

A place to put my proof of concepts or random code snippets before they go on
to live in a more permanent location.
